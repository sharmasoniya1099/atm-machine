package ATM;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Account {

		Scanner Input =new Scanner(System.in);
		DecimalFormat moneyFormat=new DecimalFormat("'$'###,##0.00");
		
		public int setCustomerNumber(int customerNumber) {
			this.customerNumber = customerNumber;
			return customerNumber;
		}
		public int getCustomerNumber() {
			return customerNumber;
		}
		public int setPinNumber(int pinNumber) {
			this.pinNumber = pinNumber;
			return pinNumber;
		}
		public int getPinNumber() {
			return pinNumber;
		}
		public double getCheckingBalance() {
			return checkingBalance;
		}
		public double getSavingBalance() {
			return savingBalance;
		}
		public double calcCheckingWithdraw(double amount) {
			checkingBalance = (checkingBalance - amount);
			return checkingBalance;
		}
		public double calcSavingWithdraw(double amount) {
			savingBalance = (savingBalance - amount);
			return savingBalance;
		}
		public double calcCheckingDeposit(double amount) {
			checkingBalance = (checkingBalance + amount);
			return checkingBalance;
		}
		public double calcSavingDeposit(double amount) {
			savingBalance = (savingBalance + amount);
			return savingBalance;
		}
		public void getCheckingWithdrawInput() {
			System.out.println("Checking Account Balance :"+ moneyFormat.format(checkingBalance));
			System.out.println("Amount you want to withdraw from Checking Account:");
			double amount = Input.nextDouble();
			
			if((checkingBalance - amount) >= 0) {
				calcCheckingWithdraw(amount);
				System.out.println("New Checking Account Balance :"+ moneyFormat.format(checkingBalance));
			}else {
				System.out.println("Balance can't be negative."+ "\n");
			}
		}
		public void getSavingWithdrawInput() {
			System.out.println("Saving Account Balance:" + moneyFormat.format(savingBalance));
			System.out.println("Amount you want to withdraw from Saving Account: ");
			double amount =Input.nextDouble();
			if((savingBalance - amount) >= 0) {
				calcSavingWithdraw(amount);
				System.out.println("New Saving Account balnace:" + savingBalance + "\n");
			}else {
				System.out.println("Balance can't be negative." + "\n");
			}
		}
		public void getCheckingDepositInput() {
			System.out.println("Checking Account balance :"+ moneyFormat.format(checkingBalance));
			System.out.println("Amount you want to be deposit from checking Accounts :");
			double amount = Input.nextDouble();
			if((checkingBalance + amount ) >= 0) {
				calcCheckingDeposit(amount);
				System.out.println("new checking account balance :"+ moneyFormat.format(checkingBalance));
			}else {
				System.out.println("Balance cant be negative:" + "\n");
			}
		}
		public void getSavingDepositInput() {
			System.out.println("saving account balance:"+ moneyFormat.format(savingBalance));
			System.out.println("amount you want to deposit from saving account:");
			double amount = Input.nextDouble();
			if((savingBalance + amount) >= 0) {
				calcSavingDeposit(amount);
				System.out.println("new saving account balance :" + moneyFormat.format(savingBalance));
			}else {
				System.out.println("balance cant be negative :" + "\n");
			}
		}
		private int customerNumber;
		private int pinNumber;
		private double checkingBalance=0;
		private double savingBalance=0;
}
